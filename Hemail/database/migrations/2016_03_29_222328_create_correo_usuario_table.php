<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorreoUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correos_usuarios',function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_correo');
            $table->integer('correo_usu_log');
            $table->integer('correo_para');
            $table->foreign('id_correo')->references('id')->on('corrreos');
            $table->foreign('correo_usu_log')->references('id')->on('users');
            $table->foreign('correo_para')->references('id')->on('users');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('correos_usuarios');
    }
}
