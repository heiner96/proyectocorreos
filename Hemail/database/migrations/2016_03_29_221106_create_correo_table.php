<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorreoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corrreos',function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('cuerpo');
            $table->string('para',69);
            $table->string('asunto',40)->nullable();
            $table->string('estado',10)->default('borrador');
        }
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correos');
    }
}
