@extends('layouts.app')
    @section('content')
        <div class="container">
          <h2>Correos Enviados</h2>      
          <table class="table">
            <thead>
              <tr>
                <th>Para</th>
                <th>Asunto</th>
                <th>Mensaje</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
            @foreach($enviados_list as $enviados)
            <tr>
                <td>{{$enviados->para}}</td>
                <td>{{$enviados->asunto}}</td>
                <td>{{$enviados->cuerpo}}</td>
                <td><a href="/enviados/{{$enviados->id}}" class="glyphicon glyphicon-eye-open"></a></td>
            </tr>
            @endforeach
              <tr>
                
              </tr>
            </tbody>
          </table>
</div>
    @endsection