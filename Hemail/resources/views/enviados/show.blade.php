@extends('layouts.app')
@section('content')
    <div class="container">
            <form role="form">
              <input TYPE="hidden" name="_token" value="{{csrf_token()}}">
                <h1>Ver contenido de un correo</h1>
                <div class="form-group">
                    <label>Para:</label>
                    <input type="email" name="to" class="from-control" value="{{$enviado[0]->para}}" disabled="true">
                </div>
                <div class="form-group">
                    <label>Asunto</label>
                    <input type="text" name="subject" class="from-control" value="{{$enviado[0]->asunto}}" disabled="true">
                </div>            
                <div class="form-group" >
                    <label>Contenido</label>
                    <textarea  name="message" class="from-control" value="{{$enviado[0]->cuerpo}}" disabled="true"></textarea>              
                </div>
                <a href="/enviados" class="btn btn-deafult">Cancel</a>
            </form>
    </div>
@endsection