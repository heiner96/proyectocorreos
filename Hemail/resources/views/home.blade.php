@extends('layouts.app')
    @section('content')
        <div class="container">
          <h2>Correos Salida</h2>      
          <table class="table">
            <thead>
              <tr>
                <th>Para</th>
                <th>Asunto</th>
                <th>Mensaje</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
            @foreach($salida_list as $salida)
            <tr>
                <td>{{$salida->para}}</td>
                <td>{{$salida->asunto}}</td>
                <td>{{$salida->cuerpo}}</td>
                <td>
                   <a href="/salida/{{$salida->id}}" class="glyphicon glyphicon-pencil"></a>
                        <form action="/salida/{{$salida->id}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{method_field('DELETE')}}
                            <button type="submit" ><i class="mdi-action-delete"></i></button>
                        </form>
                </td>
            </tr>
            @endforeach
              <tr>
                
              </tr>
            </tbody>
          </table>
</div>
    @endsection