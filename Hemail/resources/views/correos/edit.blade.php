@extends('layouts.app')
@section('content')
    <div>
            {{Form::open(array('url' => '/correo/'.$correo_edit[0]->id,'action' => 'CorreoController@update','method' => 'PUT', 'role'=>'form'))}}
            <input TYPE="hidden" name="_token" value="{{csrf_token()}}">
            <h1>Editar correo</h1>
            <div>
                <label>Para:</label>
                <input type="email" name="to" class="from-control" value="{{$correo_edit[0]->para}}">
            </div>
            <div>
                <label>Asunto</label>
                <input type="text" name="subject" class="from-control" value="{{$correo_edit[0]->asunto}}">
            </div>            
            <div>
                <label>Contenido</label>
                <input type="text" name="message" class="from-control" value="{{$correo_edit[0]->cuerpo}}">
                <button type="submit" class="btn btn-default" >Guardar</button>
            </div>
            {{Form::close()}}
    </div>
@endsection