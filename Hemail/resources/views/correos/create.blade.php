@extends('layouts.app')
    @section('content')
        <div>
            <form role="form" action="/correo" method="post">
                <input TYPE="hidden" name="_token" value="{{csrf_token()}}">
                <h1>Agregar Correo</h1>
                <div>
                    <label>Para:</label>
                    <input type="email" name="to" class="from-control">
                </div>
                <div>
                    <label>De:</label>
                    <input type="text" name="from" class="from-control">
                </div>
                <div>
                    <label>Asunto</label>
                    <input type="text" name="subject" class="from-control">

                </div>
                <div>
                    <label>Comunicado</label>
                    <textarea name="message" class="from-control"></textarea>
                </div>
                <div>
                    <button type="submit" class="btn btn-default" >Guardar</button>
                </div>

            </form>
        </div>
    @endsection