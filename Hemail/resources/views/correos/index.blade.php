@extends('layouts.app')
    @section('content')
        <div class="container">
          <h2>Correos en Borrador</h2>      
          <table class="table">
            <thead>
              <tr>
                <th>Para</th>
                <th>Asunto</th>
                <th>Mensaje</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
            @foreach($drafted_list as $draft)
            <tr>
                <td>{{$draft->para}}</td>
                <td>{{$draft->asunto}}</td>
                <td>{{$draft->cuerpo}}</td>
                <td>
                   <a href="/correo/{{$draft->id}}/edit" class="glyphicon glyphicon-pencil"></a>
                   <a href="/correo/{{$draft->id}}" class="glyphicon glyphicon-ok"></a>
                        <form action="/correo/{{$draft->id}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{method_field('DELETE')}}
                            <button type="submit" ><i class="mdi-action-delete"></i></button>
                        </form>
                </td>
            </tr>
            @endforeach
              <tr>
                
              </tr>
            </tbody>
          </table>
</div>
    @endsection