<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::group(['middleware'=>'web'],function(){
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::resource('/correo', 'CorreoController',['except'=>['show']]);
    Route::resource('/enviados', 'EnviadoController',['except'=>['create','edit','store','destroy','update']]);
    Route::resource('/salida', 'SalidaController',['except'=>['create','store','show']]);
    Route::get('correo/{id}','CorreoController@draftToSend');
});
