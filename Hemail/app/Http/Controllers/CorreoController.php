<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Correo as correo;
use App\Correos_Usuarios as correos_us;
use Auth;
class CorreoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $draft = new correos_us();
        $draft = $draft->loadDraftMail(Auth::user()->id);
        return view('correos.index')->with('drafted_list',$draft);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('correos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nuevo_correo = new correo();
        $nuevo_correo->saveCorreo($request);
        return redirect('/correo/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $correo_edit = new correo();
        $correo_edit=$correo_edit->getCorreo($id);
        return view('correos.edit')->with('correo_edit', $correo_edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit_corre = new correo();
        if($edit_corre->editCorreo($id,$request->all())){
            return redirect('/correo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nuevo_correo = new correo();
       if ($nuevo_correo->deleteCorreo($id)) {
           return back();
       }
    }
    public function draftToSend($id)
    {
        $correo= new correo();
        if($correo->cambiar_Estado($id))
        {
            return redirect('/correo');
        }
    }
}
