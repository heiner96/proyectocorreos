<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Correos_Usuarios as correos_us;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salida = new correos_us();
        $salida = $salida->loadSalidaMail(Auth::user()->id);        
        return view('home')->with('salida_list',$salida);
    }
}
