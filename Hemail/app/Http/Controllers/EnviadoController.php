<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Correo as correo;
use App\Correos_Usuarios as correos_us;
use Auth;

class EnviadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enviados = new correos_us();
        $enviados = $enviados->loadEnviadosMail(Auth::user()->id);
        return view('enviados.index')->with('enviados_list',$enviados);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enviados = new correo();
        $enviados = $enviados->getCorreo($id);
        return view('enviados.show')->with('enviado',$enviados);
    }

}
