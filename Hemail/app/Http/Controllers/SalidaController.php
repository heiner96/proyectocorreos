<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Correo as correo;
use App\Correos_Usuarios as correos_us;
use Auth;

class SalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salida = new correos_us();
        $salida = $salida->loadSalidaMail(Auth::user()->id);
        return view('salida.index')->with('salida_list',$salida);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $correo_edit = new correo();
        $correo_edit=$correo_edit->getCorreo($id);
        return view('correos.edit')->with('correo_edit', $correo_edit);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit_corre = new correo();
        if($edit_corre->editCorreo($id,$request->all())){
            return redirect('/salida');
        }    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $nuevo_correo = new correo();
       if ($nuevo_correo->deleteCorreo($id)) {
           return back();
       }
    }
}
