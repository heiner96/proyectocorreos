<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Correos_Usuarios extends Model
{
    //
    public function saveCorreo_USuarios($idCorreo, $log_user_id,$correo_para){
        $correo_Usuarios = DB::table('correos_usuarios')->insert([
            'id_correo'=>$idCorreo,
            'correo_usu_log'=>$log_user_id,
            'correo_para'=>$correo_para
        ]);
        return $correo_Usuarios;
    }

    public function loadDraftMail($auth_user_id){
    	$draft_list = DB::table('correos_usuarios')
    			->join('users','correos_usuarios.correo_usu_log','=','users.id')
    			->join('corrreos','correos_usuarios.id_correo','=','corrreos.id')
    			->where([
    				'correos_usuarios.correo_usu_log'=>$auth_user_id,
    				'corrreos.estado'=>'borrador',
    				])
    			->select('corrreos.para','corrreos.cuerpo','corrreos.asunto','corrreos.id')
    			->get();
    	return $draft_list;			
    }
      public function loadEnviadosMail($auth_user_id){
        $draft_list = DB::table('correos_usuarios')
                ->join('users','correos_usuarios.correo_usu_log','=','users.id')
                ->join('corrreos','correos_usuarios.id_correo','=','corrreos.id')
                ->where([
                    'correos_usuarios.correo_usu_log'=>$auth_user_id,
                    'corrreos.estado'=>'enviados',
                    ])
                ->select('corrreos.para','corrreos.cuerpo','corrreos.asunto','corrreos.id')
                ->get();
        return $draft_list;         
    }
    public function loadSalidaMail($auth_user_id){
        $draft_list = DB::table('correos_usuarios')
                ->join('users','correos_usuarios.correo_usu_log','=','users.id')
                ->join('corrreos','correos_usuarios.id_correo','=','corrreos.id')
                ->where([
                    'correos_usuarios.correo_usu_log'=>$auth_user_id,
                    'corrreos.estado'=>'salida',
                    ])
                ->select('corrreos.para','corrreos.cuerpo','corrreos.asunto','corrreos.id')
                ->get();
        return $draft_list;         
    }

    public function deleteCorreo_Usu($id)
    {
    	$record =DB::table('correos_usuarios')->where('id_correo',$id)->delete();
    	return $record;
    }
}
