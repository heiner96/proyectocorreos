<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Mail;
use App\Correos_Usuarios as correo_user;
use App\User as usuario;
class Correo extends Model
{
    
    protected $fillable=['cuerpo','para','asunto'];
    protected  $hidden=['id','estado'];

    public function saveCorreo($request){
        $usuario = new usuario();
        $correo_usua= new correo_user();
        $correo = DB::table('corrreos')->insertGetId([
            'cuerpo'=>$request->message,
            'para'=>$request->to,
            'asunto'=>$request->subject
        ]);
        $usuario=$usuario->getIdBYCorreo($request->to);
        $correo_usua->saveCorreo_USuarios($correo,Auth::user()->id,intval($usuario));
        return $correo;
    }

     public function getCorreo($id)
    {
        $correo=DB::table('corrreos')->where('id',$id)->get();
        return $correo;
    }

    public function editCorreo($id, $request)
    {
        $correo=DB::table('corrreos')->where('id',$id)
            ->update([
            'cuerpo'=>$request['message'],
            'para'=>$request['to'],
            'asunto'=>$request['subject'],
        ]);
        return $correo;
    }

    public function deleteCorreo($id)
    {
        $correo_usua= new correo_user();
        if($correo_usua->deleteCorreo_Usu($id)){
            $delete = DB::table('corrreos')->where('id',$id)->delete();
        }
        return $delete; 
    }
        //de borrador a salida y de salida a enviado
    public function cambiar_Estado($id)
    {
        $correo=DB::table('corrreos')->where('id',$id)->select('estado')->get();
        $state=0;
        if($correo[0]->estado=='borrador')
        {
           $state=  DB::table('corrreos')->where('id',$id)->update(['estado'=>'salida']);
        }
        
        if($correo[0]->estado=='salida'){
              $state=    DB::table('corrreos')->where('id',$id)->update(['estado'=>'enviados']);
        }
        return $state;
    }

    public function EnviarCorreos($request)
    {
            $paraVista = array('paraVista'=> $request->cuerpo);
        $enviar = Mail::send('sendmails.message',$paraVista, function ($message) use ($request) {
                    $message->from($request->para,'Adminstracion');
                    $message->to($request->para);
                    $message->subject($request->asunto);
                });
        return $enviar;
    }

    public function traerCorreosSalida()
    {
        $listaSalida= DB::table('corrreos')->where('estado','salida')->get();
        return $listaSalida;
    }


}
