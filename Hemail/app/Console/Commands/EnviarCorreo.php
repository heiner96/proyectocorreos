<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Correo as correos;
class EnviarCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Esto envia todos los correos del sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obj_correo = new correos();
        $correos_salida = $obj_correo->traerCorreosSalida();
        if($correos_salida != null){
                foreach ($correos_salida as $correoSa) {
                    $obj_correo->EnviarCorreos($correoSa);
                    $obj_correo->cambiar_Estado($correoSa->id);
                }
          $this->info('Se han enviando todos los correos');      
        }
        else{
            $this->info('No hay correos pendientes');   
        }
    }
}
